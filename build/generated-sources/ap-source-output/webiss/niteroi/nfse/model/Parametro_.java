package webiss.niteroi.nfse.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-11-20T19:58:54")
@StaticMetamodel(Parametro.class)
public class Parametro_ { 

    public static volatile SingularAttribute<Parametro, Long> id;
    public static volatile SingularAttribute<Parametro, String> nomeCertificadoJKS;
    public static volatile SingularAttribute<Parametro, String> senhaCertificado;
    public static volatile SingularAttribute<Parametro, Integer> isAtivo;

}